use crate::key::Key;

pub(crate) const KEYS_PER_LAYER: usize = 64;

#[derive(Debug)]
pub(crate) struct Layer {
    pub id: u8,
    pub name: String,
    pub keys: Vec<Key>, // TODO(adma): Should this be an array?
}

/* Key indices */
/*
_____________________                 _____________________
|  |  |  |  |  |  |  |               |  |  |  |  |  |  |  |
| 0| 1| 2| 3| 4| 5| 6|               |32|33|34|35|36|37|38|
|__|__|__|__|__|__|__|               |__|__|__|__|__|__|__|
|  |  |  |  |  |  |  |               |  |  |  |  |  |  |  |
| 7| 8| 9|10|11|12|  |               |  |40|41|42|43|44|45|
|__|__|__|__|__|__|13|               |39|__|__|__|__|__|__|
|  |  |  |  |  |  |  |               |  |  |  |  |  |  |  |
|14|15|16|17|18|19|  |               |  |46|47|48|49|50|51|
|__|__|__|__|__|__|__|               |__|__|__|__|__|__|__|
|  |  |  |  |  |  |  |               |  |  |  |  |  |  |  |
|20|21|22|23|24|25|26|               |52|53|54|55|56|57|58|
|__|__|__|__|__|__|__|_____     _____|__|__|__|__|__|__|__|
               |  |  |  |  |   |  |  |  |  |
               |27|28|29|30|   |59|60|61|62|
               |__|__|__|__|   |__|__|__|__|
                    |    |       |    |
                    | 31 |       | 63 |
                    |____|       |____|
*/

impl Layer {
    pub(crate) fn to_kle_json(&self) -> String {
        format!("[{}]", self.to_kle_raw_data())
    }

    pub(crate) fn to_kle_raw_data(&self) -> String {
        format!(
            concat!(
                r#"["{0}","{1}","{2}","{3}","{4}","{5}",{{"h":1.33}},"{6}",{{"x":1.5,"h":1.33}},"{32}","{33}","{34}","{35}","{36}","{37}","{38}"],"#,
                "\n",
                r#"["{7}","{8}","{9}","{10}","{11}","{12}",{{"x":3.5}},"{40}","{41}","{42}","{43}","{44}","{45}"],"#,
                "\n",
                r#"[{{"y":-0.67,"x":6,"h":1.33}},"{13}",{{"x":1.5,"h":1.33}},"{39}"],"#,
                "\n",
                r#"[{{"y":-0.33}},"{14}","{15}","{16}","{17}","{18}","{19}",{{"x":3.5}},"{46}","{47}","{48}","{49}","{50}","{51}"],"#,
                "\n",
                r#"[{{"y":-0.33,"x":6,"h":1.33}},"{26}",{{"x":1.5,"h":1.33}},"{52}"],"#,
                "\n",
                r#"[{{"y":-0.67}},"{20}","{21}","{22}","{23}","{24}","{25}",{{"x":3.5}},"{53}","{54}","{55}","{56}","{57}","{58}"],"#,
                "\n",
                r#"[{{"y":0.25,"x":3.5}},"{27}","{28}","{29}","{30}",{{"x":0.5}},"{59}","{60}","{61}","{62}"],"#,
                "\n",
                r#"[{{"y":0.25,"x":5.25,"w":1.25,"h":1.25}},"{31}",{{"x":0.25,"a":6,"w":2,"d":true}},"{64}",{{"x":0.25,"a":4,"w":1.25,"h":1.25}},"{63}"]"#
            ),
            self.keys[0],
            self.keys[1],
            self.keys[2],
            self.keys[3],
            self.keys[4],
            self.keys[5],
            self.keys[6],
            self.keys[7],
            self.keys[8],
            self.keys[9],
            self.keys[10],
            self.keys[11],
            self.keys[12],
            self.keys[13],
            self.keys[14],
            self.keys[15],
            self.keys[16],
            self.keys[17],
            self.keys[18],
            self.keys[19],
            self.keys[20],
            self.keys[21],
            self.keys[22],
            self.keys[23],
            self.keys[24],
            self.keys[25],
            self.keys[26],
            self.keys[27],
            self.keys[28],
            self.keys[29],
            self.keys[30],
            self.keys[31],
            self.keys[32],
            self.keys[33],
            self.keys[34],
            self.keys[35],
            self.keys[36],
            self.keys[37],
            self.keys[38],
            self.keys[39],
            self.keys[40],
            self.keys[41],
            self.keys[42],
            self.keys[43],
            self.keys[44],
            self.keys[45],
            self.keys[46],
            self.keys[47],
            self.keys[48],
            self.keys[49],
            self.keys[50],
            self.keys[51],
            self.keys[52],
            self.keys[53],
            self.keys[54],
            self.keys[55],
            self.keys[56],
            self.keys[57],
            self.keys[58],
            self.keys[59],
            self.keys[60],
            self.keys[61],
            self.keys[62],
            self.keys[63],
            self.name
        )
    }
}
