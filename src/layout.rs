use regex::Regex;
use std::str::FromStr;

use crate::key::Key;
use crate::layer::Layer;

const REGEX: &str = r"\[(\w*)\]=KEYMAP_STACKED\(((?:\w*(?:\(\w*\))?,?)+)\)";

#[derive(Debug)]
pub(crate) struct Layout {
    pub(crate) layers: Vec<Layer>,
}

impl FromStr for Layout {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let layout_string = s.chars().filter(|x| !x.is_whitespace()).collect::<String>();

        let re = Regex::new(REGEX)?;

        let mut vec = Vec::new();

        for (id, caps) in re.captures_iter(&layout_string).enumerate() {
            let name = caps.get(1).unwrap().as_str().to_owned();
            let keys = caps.get(2).unwrap().as_str();

            let keys = keys
                .split(',')
                .map(|x| x.parse().unwrap())
                .collect::<Vec<Key>>();

            if keys.len() != crate::layer::KEYS_PER_LAYER {
                return Err(anyhow::anyhow!(
                    "Unable to parse layer {}, found {} keys",
                    name,
                    keys.len()
                ));
            }

            vec.push(Layer {
                id: id as u8,
                name,
                keys,
            });
        }

        Ok(Layout { layers: vec })
    }
}
