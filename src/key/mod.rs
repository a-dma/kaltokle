use std::fmt;
use std::str::FromStr;

mod aliases;
mod consumer_ctl;
mod keyboard;
mod keymaps;
mod led_control;
mod macros;
mod mouse_keys;
mod special;
mod system_ctl;

use self::aliases::Aliases;
use self::consumer_ctl::ConsumerCtl;
use self::keyboard::Keyboard;
use self::keymaps::Keymaps;
use self::led_control::LEDControl;
use self::macros::Macros;
use self::mouse_keys::MouseKeys;
use self::special::Special;
use self::system_ctl::SystemCtl;

#[derive(Debug)]
pub(crate) enum Key {
    Aliases(aliases::Aliases),
    ConsumerCtl(consumer_ctl::ConsumerCtl),
    Keyboard(keyboard::Keyboard),
    Keymaps(keymaps::Keymaps),
    LEDControl(led_control::LEDControl),
    Macros(macros::Macros),
    MouseKeys(mouse_keys::MouseKeys),
    Special(special::Special),
    SystemCtl(system_ctl::SystemCtl),
}

impl fmt::Display for Key {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Key::Aliases(x) => write!(f, "{}", x),
            Key::ConsumerCtl(x) => write!(f, "{}", x),
            Key::Keyboard(x) => write!(f, "{}", x),
            Key::Keymaps(x) => write!(f, "{}", x),
            Key::LEDControl(x) => write!(f, "{}", x),
            Key::Macros(x) => write!(f, "{}", x),
            Key::MouseKeys(x) => write!(f, "{}", x),
            Key::Special(x) => write!(f, "{}", x),
            Key::SystemCtl(x) => write!(f, "{}", x),
        }
    }
}

impl FromStr for Key {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(key) = s.parse::<Aliases>() {
            return Ok(Key::Aliases(key));
        }

        if let Ok(key) = s.parse::<ConsumerCtl>() {
            return Ok(Key::ConsumerCtl(key));
        }

        if let Ok(key) = s.parse::<Keyboard>() {
            return Ok(Key::Keyboard(key));
        }

        if let Ok(key) = s.parse::<Keymaps>() {
            return Ok(Key::Keymaps(key));
        }

        if let Ok(key) = s.parse::<LEDControl>() {
            return Ok(Key::LEDControl(key));
        }

        if let Ok(key) = s.parse::<Macros>() {
            return Ok(Key::Macros(key));
        }

        if let Ok(key) = s.parse::<MouseKeys>() {
            return Ok(Key::MouseKeys(key));
        }

        if let Ok(key) = s.parse::<Special>() {
            return Ok(Key::Special(key));
        }

        if let Ok(key) = s.parse::<SystemCtl>() {
            return Ok(Key::SystemCtl(key));
        }

        Err(format!("Unknown key {}", s))
    }
}
