use std::fmt;
use std::str::FromStr;

// Kaleidoscope/src/key_defs_sysctl.h

#[cfg_attr(feature = "cargo-clippy", allow(clippy::enum_variant_names))]
#[allow(non_camel_case_types)]
#[derive(Debug)]
pub(crate) enum SystemCtl {
    System_PowerDown,
    System_Sleep,
    System_WakeUp,
    System_ContextMenu,
    System_MainMenu,
    System_AppMenu,
    System_MenuHelp,
    System_MenuExit,
    System_MenuSelect,
    System_MenuRight,
    System_MenuLeft,
    System_MenuUp,
    System_MenuDown,
    System_ColdRestart,
    System_WarmRestart,
    System_DPadUp,
    System_DPadDown,
    System_DPadRight,
    System_DPadLeft,
    System_Dock,
    System_Undock,
    System_Setup,
    System_Break,
    System_DebuggerBreak,
    System_ApplicationBreak,
    System_ApplicationDebuggerBreak,
    System_SpeakerMute,
    System_Hibernate,
    System_DisplayInvert,
    System_DisplayInternal,
    System_DisplayExternal,
    System_DisplayBoth,
    System_DisplayDual,
    System_DisplayToggleIntSlashExt,
    System_DisplaySwapPrimarySlashSecondary,
    System_DisplayLCDAutoscale,
}

impl fmt::Display for SystemCtl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SystemCtl::System_PowerDown => write!(f, "Power Down"),
            SystemCtl::System_Sleep => write!(f, "Sleep"),
            SystemCtl::System_WakeUp => write!(f, "Wake up"),
            SystemCtl::System_ContextMenu => write!(f, "Context Menu"),
            SystemCtl::System_MainMenu => write!(f, "Main Menu"),
            SystemCtl::System_AppMenu => write!(f, "App Menu"),
            SystemCtl::System_MenuHelp => write!(f, "Menu Help"),
            SystemCtl::System_MenuExit => write!(f, "Menu Exit"),
            SystemCtl::System_MenuSelect => write!(f, "Menu Select"),
            SystemCtl::System_MenuRight => write!(f, "Menu Right"),
            SystemCtl::System_MenuLeft => write!(f, "Menu Left"),
            SystemCtl::System_MenuUp => write!(f, "Menu Up"),
            SystemCtl::System_MenuDown => write!(f, "Menu Down"),
            SystemCtl::System_ColdRestart => write!(f, "Cold Restart"),
            SystemCtl::System_WarmRestart => write!(f, "Warm Restart"),
            SystemCtl::System_DPadUp => write!(f, "DPad Up"),
            SystemCtl::System_DPadDown => write!(f, "DPad Down"),
            SystemCtl::System_DPadRight => write!(f, "DPad Right"),
            SystemCtl::System_DPadLeft => write!(f, "DPad Left"),
            SystemCtl::System_Dock => write!(f, "Dock"),
            SystemCtl::System_Undock => write!(f, "Undock"),
            SystemCtl::System_Setup => write!(f, "Setup"),
            SystemCtl::System_Break => write!(f, "Break"),
            SystemCtl::System_DebuggerBreak => write!(f, "Debugger Break"),
            SystemCtl::System_ApplicationBreak => write!(f, "Application Break"),
            SystemCtl::System_ApplicationDebuggerBreak => write!(f, "Application Debugger Break"),
            SystemCtl::System_SpeakerMute => write!(f, "Speaker Mute"),
            SystemCtl::System_Hibernate => write!(f, "Hibernate"),
            SystemCtl::System_DisplayInvert => write!(f, "Display Invert"),
            SystemCtl::System_DisplayInternal => write!(f, "Display Internal"),
            SystemCtl::System_DisplayExternal => write!(f, "Display Extenal"),
            SystemCtl::System_DisplayBoth => write!(f, "Display Both"),
            SystemCtl::System_DisplayDual => write!(f, "Display Dual"),
            SystemCtl::System_DisplayToggleIntSlashExt => write!(f, "Display Toggle Int/Ext"),
            SystemCtl::System_DisplaySwapPrimarySlashSecondary => write!(f, "Display Swap Pri/Sec"),
            SystemCtl::System_DisplayLCDAutoscale => write!(f, "LCD Autoscale"),
        }
    }
}

impl FromStr for SystemCtl {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "System_PowerDown" => Ok(SystemCtl::System_PowerDown),
            "System_Sleep" => Ok(SystemCtl::System_Sleep),
            "System_WakeUp" => Ok(SystemCtl::System_WakeUp),
            "System_ContextMenu" => Ok(SystemCtl::System_ContextMenu),
            "System_MainMenu" => Ok(SystemCtl::System_MainMenu),
            "System_AppMenu" => Ok(SystemCtl::System_AppMenu),
            "System_MenuHelp" => Ok(SystemCtl::System_MenuHelp),
            "System_MenuExit" => Ok(SystemCtl::System_MenuExit),
            "System_MenuSelect" => Ok(SystemCtl::System_MenuSelect),
            "System_MenuRight" => Ok(SystemCtl::System_MenuRight),
            "System_MenuLeft" => Ok(SystemCtl::System_MenuLeft),
            "System_MenuUp" => Ok(SystemCtl::System_MenuUp),
            "System_MenuDown" => Ok(SystemCtl::System_MenuDown),
            "System_ColdRestart" => Ok(SystemCtl::System_ColdRestart),
            "System_WarmRestart" => Ok(SystemCtl::System_WarmRestart),
            "System_DPadUp" => Ok(SystemCtl::System_DPadUp),
            "System_DPadDown" => Ok(SystemCtl::System_DPadDown),
            "System_DPadRight" => Ok(SystemCtl::System_DPadRight),
            "System_DPadLeft" => Ok(SystemCtl::System_DPadLeft),
            "System_Dock" => Ok(SystemCtl::System_Dock),
            "System_Undock" => Ok(SystemCtl::System_Undock),
            "System_Setup" => Ok(SystemCtl::System_Setup),
            "System_Break" => Ok(SystemCtl::System_Break),
            "System_DebuggerBreak" => Ok(SystemCtl::System_DebuggerBreak),
            "System_ApplicationBreak" => Ok(SystemCtl::System_ApplicationBreak),
            "System_ApplicationDebuggerBreak" => Ok(SystemCtl::System_ApplicationDebuggerBreak),
            "System_SpeakerMute" => Ok(SystemCtl::System_SpeakerMute),
            "System_Hibernate" => Ok(SystemCtl::System_Hibernate),
            "System_DisplayInvert" => Ok(SystemCtl::System_DisplayInvert),
            "System_DisplayInternal" => Ok(SystemCtl::System_DisplayInternal),
            "System_DisplayExternal" => Ok(SystemCtl::System_DisplayExternal),
            "System_DisplayBoth" => Ok(SystemCtl::System_DisplayBoth),
            "System_DisplayDual" => Ok(SystemCtl::System_DisplayDual),
            "System_DisplayToggleIntSlashExt" => Ok(SystemCtl::System_DisplayToggleIntSlashExt),
            "System_DisplaySwapPrimarySlashSecondary" => {
                Ok(SystemCtl::System_DisplaySwapPrimarySlashSecondary)
            }
            "System_DisplayLCDAutoscale" => Ok(SystemCtl::System_DisplayLCDAutoscale),
            x => Err(format!("Unknown key {}", x)),
        }
    }
}
