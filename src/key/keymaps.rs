use std::fmt;
use std::str::FromStr;

// Kaleidoscope/src/key_defs_keymaps.h

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub(crate) enum Keymaps {
    Key_Keymap0,
    Key_Keymap1,
    Key_Keymap2,
    Key_Keymap3,
    Key_Keymap4,
    Key_Keymap5,
    Key_Keymap0_Momentary,
    Key_Keymap1_Momentary,
    Key_Keymap2_Momentary,
    Key_Keymap3_Momentary,
    Key_Keymap4_Momentary,
    Key_Keymap5_Momentary,
    Key_KeymapNext_Momentary,
    Key_KeymapPrevious_Momentary,
    LockLayer(String),
    UnlockLayer(String),
    ShiftToLayer(String),
}

impl fmt::Display for Keymaps {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Keymaps::Key_Keymap0 => write!(f, "L0"),
            Keymaps::Key_Keymap1 => write!(f, "L1"),
            Keymaps::Key_Keymap2 => write!(f, "L2"),
            Keymaps::Key_Keymap3 => write!(f, "L3"),
            Keymaps::Key_Keymap4 => write!(f, "L4"),
            Keymaps::Key_Keymap5 => write!(f, "L5"),
            Keymaps::Key_Keymap0_Momentary => write!(f, "l0"),
            Keymaps::Key_Keymap1_Momentary => write!(f, "l1"),
            Keymaps::Key_Keymap2_Momentary => write!(f, "l2"),
            Keymaps::Key_Keymap3_Momentary => write!(f, "l3"),
            Keymaps::Key_Keymap4_Momentary => write!(f, "l4"),
            Keymaps::Key_Keymap5_Momentary => write!(f, "l5"),
            Keymaps::Key_KeymapNext_Momentary => write!(f, "l+"),
            Keymaps::Key_KeymapPrevious_Momentary => write!(f, "l-"),
            Keymaps::LockLayer(n) => write!(f, "LL\\n{}", n),
            Keymaps::UnlockLayer(n) => write!(f, "UL\\n{}", n),
            Keymaps::ShiftToLayer(n) => write!(f, "SL\\n{}", n),
        }
    }
}

impl FromStr for Keymaps {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("LockLayer(") && s.ends_with(')') {
            let n = &s["LockLayer(".len()..s.len() - 1];

            return Ok(Keymaps::LockLayer(n.to_owned()));
        }

        if s.starts_with("UnlockLayer(") && s.ends_with(')') {
            let n = &s["UnlockLayer(".len()..s.len() - 1];

            return Ok(Keymaps::UnlockLayer(n.to_owned()));
        }

        if s.starts_with("ShiftToLayer(") && s.ends_with(')') {
            let n = &s["ShiftToLayer(".len()..s.len() - 1];

            return Ok(Keymaps::ShiftToLayer(n.to_owned()));
        }

        match s {
            "Key_Keymap0" => Ok(Keymaps::Key_Keymap0),
            "Key_Keymap1" => Ok(Keymaps::Key_Keymap1),
            "Key_Keymap2" => Ok(Keymaps::Key_Keymap2),
            "Key_Keymap3" => Ok(Keymaps::Key_Keymap3),
            "Key_Keymap4" => Ok(Keymaps::Key_Keymap4),
            "Key_Keymap5" => Ok(Keymaps::Key_Keymap5),
            "Key_Keymap0_Momentary" => Ok(Keymaps::Key_Keymap0_Momentary),
            "Key_Keymap1_Momentary" => Ok(Keymaps::Key_Keymap1_Momentary),
            "Key_Keymap2_Momentary" => Ok(Keymaps::Key_Keymap2_Momentary),
            "Key_Keymap3_Momentary" => Ok(Keymaps::Key_Keymap3_Momentary),
            "Key_Keymap4_Momentary" => Ok(Keymaps::Key_Keymap4_Momentary),
            "Key_Keymap5_Momentary" => Ok(Keymaps::Key_Keymap5_Momentary),
            "Key_KeymapNext_Momentary" => Ok(Keymaps::Key_KeymapNext_Momentary),
            "Key_KeymapPrevious_Momentary" => Ok(Keymaps::Key_KeymapPrevious_Momentary),
            x => Err(format!("Unknown key {}", x)),
        }
    }
}
