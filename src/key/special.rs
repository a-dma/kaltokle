use std::fmt;
use std::str::FromStr;

// Kaleidoscope/src/key_defs.h

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub(crate) enum Special {
    Key_Transparent,
    Key_NoKey,
}

impl fmt::Display for Special {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Special::Key_Transparent => write!(f, "___"),
            Special::Key_NoKey => write!(f, "XXX"),
        }
    }
}

impl FromStr for Special {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "___" | "KeyTrasparent" => Ok(Special::Key_Transparent),
            "XXX" | "KeyNoKey" => Ok(Special::Key_NoKey),
            x => Err(format!("Unknown key {}", x)),
        }
    }
}
