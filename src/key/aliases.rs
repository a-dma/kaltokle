use std::fmt;
use std::str::FromStr;

// Kaleidoscope/src/key_defs_aliases.h

#[cfg_attr(feature = "cargo-clippy", allow(clippy::enum_variant_names))]
#[allow(non_camel_case_types, non_snake_case)]
#[derive(Debug)]
pub(crate) enum Aliases {
    Key_Space,
    Key_LBracket,
    Key_LArrow,
    Key_LCtrl,
    Key_LShift,
    Key_LAlt,
    Key_LGui,
    Key_RBracket,
    Key_RArrow,
    Key_RCtrl,
    Key_RShift,
    Key_RAlt,
    Key_RGui,
    Key_Esc,
    Key_LSquareBracket,
    Key_RSquareBracket,
    Key_DnArrow,
    Key_LeftParen,
    Key_RightParen,
    Key_LeftCurlyBracket,
    Key_RightCurlyBracket,
    Key_Pipe,
}

impl fmt::Display for Aliases {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Aliases::Key_Space => write!(f, "Space"),
            Aliases::Key_LBracket => write!(f, "["),
            Aliases::Key_LArrow => write!(f, "←"),
            Aliases::Key_LCtrl => write!(f, "LCtrl"),
            Aliases::Key_LShift => write!(f, "LShift"),
            Aliases::Key_LAlt => write!(f, "LAlt"),
            Aliases::Key_LGui => write!(f, "LGui"),
            Aliases::Key_RBracket => write!(f, "]"),
            Aliases::Key_RArrow => write!(f, "→"),
            Aliases::Key_RCtrl => write!(f, "RCtrl"),
            Aliases::Key_RShift => write!(f, "RShift"),
            Aliases::Key_RAlt => write!(f, "RAlt"),
            Aliases::Key_RGui => write!(f, "RGui"),
            Aliases::Key_Esc => write!(f, "Esc"),
            Aliases::Key_LSquareBracket => write!(f, "["),
            Aliases::Key_RSquareBracket => write!(f, "]"),
            Aliases::Key_DnArrow => write!(f, "↓"),
            Aliases::Key_LeftParen => write!(f, "("),
            Aliases::Key_RightParen => write!(f, ")"),
            Aliases::Key_LeftCurlyBracket => write!(f, "{{"),
            Aliases::Key_RightCurlyBracket => write!(f, "}}"),
            Aliases::Key_Pipe => write!(f, "|"),
        }
    }
}

impl FromStr for Aliases {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Key_Space" => Ok(Aliases::Key_Space),
            "Key_LBracket" => Ok(Aliases::Key_LBracket),
            "Key_LArrow" => Ok(Aliases::Key_LArrow),
            "Key_LCtrl" => Ok(Aliases::Key_LCtrl),
            "Key_LShift" => Ok(Aliases::Key_LShift),
            "Key_LAlt" => Ok(Aliases::Key_LAlt),
            "Key_LGui" => Ok(Aliases::Key_LGui),
            "Key_RBracket" => Ok(Aliases::Key_RBracket),
            "Key_RArrow" => Ok(Aliases::Key_RArrow),
            "Key_RCtrl" => Ok(Aliases::Key_RCtrl),
            "Key_RShift" => Ok(Aliases::Key_RShift),
            "Key_RAlt" => Ok(Aliases::Key_RAlt),
            "Key_RGui" => Ok(Aliases::Key_RGui),
            "Key_Esc" => Ok(Aliases::Key_Esc),
            "Key_LSquareBracket" => Ok(Aliases::Key_LSquareBracket),
            "Key_RSquareBracket" => Ok(Aliases::Key_RSquareBracket),
            "Key_DnArrow" => Ok(Aliases::Key_DnArrow),
            "Key_LeftParen" => Ok(Aliases::Key_LeftParen),
            "Key_RightParen" => Ok(Aliases::Key_RightParen),
            "Key_LeftCurlyBracket" => Ok(Aliases::Key_LeftCurlyBracket),
            "Key_RightCurlyBracket" => Ok(Aliases::Key_RightCurlyBracket),
            "Key_Pipe" => Ok(Aliases::Key_Pipe),
            x => Err(format!("Unknown key {}", x)),
        }
    }
}
