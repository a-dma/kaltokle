use std::fmt;
use std::str::FromStr;

// Kaleidoscope-Mouse/src/Kaleidoscope-MouseKeys.h

// TODO(adma): use better names (e.g., MouseWarpNW) and re-enable
// lints. There's more of those in other keys files
#[cfg_attr(feature = "cargo-clippy", allow(clippy::enum_variant_names))]
#[allow(non_camel_case_types, non_snake_case)]
#[derive(Debug)]
pub(crate) enum MouseKeys {
    Key_mouseWarpNW,
    Key_mouseWarpN,
    Key_mouseWarpNE,
    Key_mouseWarpW,
    Key_mouseWarpIn,
    Key_mouseWarpE,
    Key_mouseWarpSW,
    Key_mouseWarpS,
    Key_mouseWarpSE,
    Key_mouseWarpEnd,
    Key_mouseUpL,
    Key_mouseUp,
    Key_mouseUpR,
    Key_mouseL,
    Key_mouseR,
    Key_mouseDnL,
    Key_mouseDn,
    Key_mouseDnR,
    Key_mouseScrollUp,
    Key_mouseScrollDn,
    Key_mouseScrollL,
    Key_mouseScrollR,
    Key_mouseBtnL,
    Key_mouseBtnM,
    Key_mouseBtnR,
}

impl fmt::Display for MouseKeys {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MouseKeys::Key_mouseWarpNW => write!(f, "🖱️ WarpNW"),
            MouseKeys::Key_mouseWarpN => write!(f, "🖱️ WarpN"),
            MouseKeys::Key_mouseWarpNE => write!(f, "🖱️ WarpNE"),
            MouseKeys::Key_mouseWarpW => write!(f, "🖱️ WarpW"),
            MouseKeys::Key_mouseWarpIn => write!(f, "🖱️ WarpIn"),
            MouseKeys::Key_mouseWarpE => write!(f, "🖱️ WarpE"),
            MouseKeys::Key_mouseWarpSW => write!(f, "🖱️ WarpSW"),
            MouseKeys::Key_mouseWarpS => write!(f, "🖱️ WarpS"),
            MouseKeys::Key_mouseWarpSE => write!(f, "🖱️ WarpSE"),
            MouseKeys::Key_mouseWarpEnd => write!(f, "🖱️ WarpEnd"),
            MouseKeys::Key_mouseUpL => write!(f, "🖱️ UpL"),
            MouseKeys::Key_mouseUp => write!(f, "🖱️ Up"),
            MouseKeys::Key_mouseUpR => write!(f, "🖱️ UpR"),
            MouseKeys::Key_mouseL => write!(f, "🖱️ L"),
            MouseKeys::Key_mouseR => write!(f, "🖱️ R"),
            MouseKeys::Key_mouseDnL => write!(f, "🖱️ DnL"),
            MouseKeys::Key_mouseDn => write!(f, "🖱️ Dn"),
            MouseKeys::Key_mouseDnR => write!(f, "🖱️ DnR"),
            MouseKeys::Key_mouseScrollUp => write!(f, "🖱️ ScrollUp"),
            MouseKeys::Key_mouseScrollDn => write!(f, "🖱️ ScrollDn"),
            MouseKeys::Key_mouseScrollL => write!(f, "🖱️ ScrollL"),
            MouseKeys::Key_mouseScrollR => write!(f, "🖱️ ScrollR"),
            MouseKeys::Key_mouseBtnL => write!(f, "🖱️ BtnL"),
            MouseKeys::Key_mouseBtnM => write!(f, "🖱️ BtnM"),
            MouseKeys::Key_mouseBtnR => write!(f, "🖱️ BtnR"),
        }
    }
}

impl FromStr for MouseKeys {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Key_mouseWarpNW" => Ok(MouseKeys::Key_mouseWarpNW),
            "Key_mouseWarpN" => Ok(MouseKeys::Key_mouseWarpN),
            "Key_mouseWarpNE" => Ok(MouseKeys::Key_mouseWarpNE),
            "Key_mouseWarpW" => Ok(MouseKeys::Key_mouseWarpW),
            "Key_mouseWarpIn" => Ok(MouseKeys::Key_mouseWarpIn),
            "Key_mouseWarpE" => Ok(MouseKeys::Key_mouseWarpE),
            "Key_mouseWarpSW" => Ok(MouseKeys::Key_mouseWarpSW),
            "Key_mouseWarpS" => Ok(MouseKeys::Key_mouseWarpS),
            "Key_mouseWarpSE" => Ok(MouseKeys::Key_mouseWarpSE),
            "Key_mouseWarpEnd" => Ok(MouseKeys::Key_mouseWarpEnd),
            "Key_mouseUpL" => Ok(MouseKeys::Key_mouseUpL),
            "Key_mouseUp" => Ok(MouseKeys::Key_mouseUp),
            "Key_mouseUpR" => Ok(MouseKeys::Key_mouseUpR),
            "Key_mouseL" => Ok(MouseKeys::Key_mouseL),
            "Key_mouseR" => Ok(MouseKeys::Key_mouseR),
            "Key_mouseDnL" => Ok(MouseKeys::Key_mouseDnL),
            "Key_mouseDn" => Ok(MouseKeys::Key_mouseDn),
            "Key_mouseDnR" => Ok(MouseKeys::Key_mouseDnR),
            "Key_mouseScrollUp" => Ok(MouseKeys::Key_mouseScrollUp),
            "Key_mouseScrollDn" => Ok(MouseKeys::Key_mouseScrollDn),
            "Key_mouseScrollL" => Ok(MouseKeys::Key_mouseScrollL),
            "Key_mouseScrollR" => Ok(MouseKeys::Key_mouseScrollR),
            "Key_mouseBtnL" => Ok(MouseKeys::Key_mouseBtnL),
            "Key_mouseBtnM" => Ok(MouseKeys::Key_mouseBtnM),
            "Key_mouseBtnR" => Ok(MouseKeys::Key_mouseBtnR),
            x => Err(format!("Unknown key {}", x)),
        }
    }
}
