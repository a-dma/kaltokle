use std::fmt;
use std::str::FromStr;

// Kaleidoscope-Macros/src/MacroKeyDefs.h

#[derive(Debug)]
pub(crate) enum Macros {
    M(String),
}

impl fmt::Display for Macros {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Macros::M(m) => write!(f, "⚙️\\n{}", m.replace("MACRO_", "")),
        }
    }
}

impl FromStr for Macros {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("M(") && s.ends_with(')') {
            let n = &s["M(".len()..s.len() - 1];

            return Ok(Macros::M(n.to_owned()));
        }

        Err(format!("Unknown key {}", s))
    }
}
