use std::fmt;
use std::str::FromStr;

// Kaleidoscope-LEDControl/src/Kaleidoscope-LEDControl.h

//#[allow(non_camel_case_types)]
#[derive(Debug)]
pub(crate) enum LEDControl {
    LEDEffectNext,
    LEDEffectPrevious,
}

impl fmt::Display for LEDControl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LEDControl::LEDEffectNext => write!(f, "LED+"),
            LEDControl::LEDEffectPrevious => write!(f, "LED-"),
        }
    }
}

impl FromStr for LEDControl {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Key_LEDEffectNext" => Ok(LEDControl::LEDEffectNext),
            "Key_LEDEffectPrevious" => Ok(LEDControl::LEDEffectPrevious),
            x => Err(format!("Unknown key {}", x)),
        }
    }
}
