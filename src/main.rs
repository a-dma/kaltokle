use std::fs::{self, File};
use std::io::prelude::*;
use std::io::{self, Write};

use anyhow::Result;
use clap::{values_t, App, Arg};

mod key;
mod layer;
mod layout;

use layer::Layer;
use layout::Layout;

#[derive(Debug)]
enum Format {
    Json,
    RawData,
    Png,
}

#[derive(Debug)]
enum Output {
    File,
    Stdout,
}

fn main() -> Result<()> {
    let matches = App::new(clap::crate_name!())
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .arg(
            Arg::with_name("format")
                .short("f")
                .long("format")
                .help("Specify which ouput format to use")
                .takes_value(true)
                .number_of_values(1)
                .possible_value("raw")
                .possible_value("json")
                .possible_value("png")
                .default_value("raw")
                .hide_default_value(false),
        )
        .arg(
            Arg::with_name("out")
                .short("o")
                .long("out")
                .help("Prints to STDOUT or file(s)")
                .takes_value(true)
                .possible_value("stdout")
                .possible_value("file")
                .possible_value("format-specific")
                .default_value("format-specific")
                .hide_default_value(false),
        )
        .arg(
            Arg::with_name("layer")
                .short("l")
                .long("layer")
                .help("Specify which layer to operate on")
                .takes_value(true)
                .number_of_values(1)
                .multiple(true)
                .hide_default_value(false),
        )
        .arg(
            Arg::with_name("list")
                .short("L")
                .long("list-layers")
                .help("List the available layers and exit")
                .conflicts_with("format"),
        )
        .arg(
            Arg::with_name("layout-file")
                .help("Layout file to parse (typically .ino)")
                .required(true),
        )
        .get_matches();

    let input_file = matches
        .value_of("layout-file")
        .ok_or_else(|| anyhow::anyhow!("missing layout-file"))?;
    let mut file = match File::open(input_file) {
        Ok(file) => file,
        Err(err) => {
            println!("Unable to open input file {}: {}", input_file, err);
            std::process::exit(1);
        }
    };

    let mut content = String::new();
    file.read_to_string(&mut content)?;

    let format = match matches.value_of("format").unwrap() {
        "raw" => Format::RawData,
        "json" => Format::Json,
        "png" => Format::Png,
        _ => unreachable!(),
    };

    let output = match matches.value_of("out").unwrap() {
        "stdout" => Output::Stdout,
        "file" => Output::File,
        "format-specific" => match format {
            Format::Json | Format::Png => Output::File,
            Format::RawData => Output::Stdout,
        },
        _ => unreachable!(),
    };

    let layout: Layout = content.parse()?;

    let mut layers: Vec<usize> = if matches.is_present("layer") {
        clap::values_t!(matches, "layer", usize)?.to_vec()
    } else {
        layout.layers.iter().enumerate().map(|(i, _)| i).collect()
    };
    layers.sort();

    for layer_id in layers {
        let layer = match layout.layers.get(layer_id) {
            Some(x) => x,
            None => {
                println!("Unable to get layer {}", layer_id);
                continue;
            }
        };

        if matches.is_present("list") {
            println!("{:2} - {}", layer_id, layer.name);
        } else {
            match output {
                Output::Stdout => print_to_stdout(layer, &format)?,
                Output::File => print_to_file(layer, &format)?,
            }
        }
    }

    Ok(())
}

fn print_to_stdout(layer: &Layer, format: &Format) -> Result<()> {
    match format {
        Format::Json => {
            println!("{}-{}:\n{}", layer.name, layer.id, layer.to_kle_json());
        }
        Format::RawData => {
            println!("{}-{}:\n{}", layer.name, layer.id, layer.to_kle_raw_data());
        }
        Format::Png => {
            io::stdout().write_all(&render(layer)?)?;
        }
    }
    Ok(())
}

fn print_to_file(layer: &Layer, format: &Format) -> Result<()> {
    match format {
        Format::Json => {
            let file_name = format!("{}-{}.json", layer.name.to_lowercase(), layer.id);
            fs::write(file_name, layer.to_kle_json())?
        }
        Format::RawData => {
            let file_name = format!("{}-{}.raw", layer.name.to_lowercase(), layer.id);
            fs::write(file_name, layer.to_kle_raw_data())?
        }
        Format::Png => {
            let file_name = format!("{}-{}.png", layer.name.to_lowercase(), layer.id);
            println!("Now rendering layer {}", layer.name.to_lowercase());
            fs::write(file_name, render(layer)?)?
        }
    }

    Ok(())
}

fn render(layer: &Layer) -> Result<Vec<u8>> {
    let client = reqwest::blocking::Client::new();

    let mut resp = client
        .post("https://kle-render.herokuapp.com/api/")
        .header(
            reqwest::header::CONTENT_TYPE,
            reqwest::header::HeaderValue::from_static("application/json"),
        )
        .body(layer.to_kle_json())
        .send()?;

    if !resp.status().is_success() {
        anyhow::bail!("Request failed: {}", resp.status());
    }

    let mut buf: Vec<u8> = vec![];
    resp.copy_to(&mut buf)?;

    Ok(buf)
}
